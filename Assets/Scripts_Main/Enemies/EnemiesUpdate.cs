using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesUpdate : MonoBehaviour
{
    private bool SkinnyCleanedUp = false;
    private bool FastCleanedUp = false;
    private bool FatCleanedUp = false;
    [SerializeField] GameObject SkinnyZombieGO;
    [SerializeField] GameObject FastZombieGO;
    [SerializeField] GameObject FatZombieGO;
    [SerializeField] GameObject OneShot;

    void Update()
    {
        if (SaveScript.EnemiesOnScreen == 2)
        {
            if (!SkinnyCleanedUp)
            {
                StartCoroutine(CleanupSkinny());
            }
        }
        if (SaveScript.EnemiesOnScreen == 1)
        {
            if (!FastCleanedUp)
            {
                StartCoroutine(CleanupFast());
            }
        }
        if (SaveScript.EnemiesOnScreen == 0)
        {
            if (!FatCleanedUp)
            {
                StartCoroutine(CleanupFat());
            }
        }
    }

    IEnumerator CleanupSkinny()
    {
        yield return new WaitForSeconds(60.0f);
        if (SkinnyZombieGO != null)
        {
            SkinnyZombieGO.gameObject.SetActive(false);
        }
        FastZombieGO.gameObject.SetActive(true);
        SkinnyCleanedUp = true;
    }
    IEnumerator CleanupFast()
    {
        yield return new WaitForSeconds(45.0f);
        if (FastZombieGO != null)
        {
            FastZombieGO.gameObject.SetActive(false);
        }
        FatZombieGO.gameObject.SetActive(true);
        FastCleanedUp = true;
    }
    IEnumerator CleanupFat()
    {
        if(OneShot != null && OneShot.gameObject != null)
            OneShot.gameObject.SetActive(true);
        
        yield return new WaitForSeconds(60.0f);
        if (FatZombieGO != null)
        {
            FatZombieGO.gameObject.SetActive(false);
        }
        FatCleanedUp = true;
    }
}
