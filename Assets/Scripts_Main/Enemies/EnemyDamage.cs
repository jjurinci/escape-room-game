using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemyDamage : MonoBehaviour
{
    public int EnemyHealth = 100;
    private AudioSource AudioPlayer;
    [SerializeField] AudioSource StabPlayer;
    public bool HasDied = false;
    private Animator Anim;
    [SerializeField] GameObject BloodSplat;
    private bool CanDamage = false;
    void Start()
    {
        AudioPlayer = GetComponent<AudioSource>();
        Anim = GetComponentInParent<Animator>();
        StartCoroutine(StartEelements());
    }

    // Update is called once per frame
    void Update()
    {
        if (CanDamage)
        {
            if (EnemyHealth <= 0)
            {
                if (!HasDied)
                {
                    Anim.SetTrigger("Death");
                    Anim.SetBool("IsDead", true);
                    HasDied = true;
                    SaveScript.EnemiesOnScreen--;
                    SaveScript.ChaseMusicPlaying = false;
                    gameObject.transform.parent.GetComponentInChildren<CapsuleCollider>().enabled = false;
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        int idx = Weapon.GetWeaponIndex(SaveScript.Weapons, other.gameObject.tag);
        if (idx != -1)
        {
            BloodSplat.gameObject.SetActive(true);
            EnemyHealth -= SaveScript.Weapons[idx].GetDamage();
            AudioPlayer.Play();
            StabPlayer.Play();
        }
    }

    IEnumerator StartEelements()
    {
        yield return new WaitForSeconds(0.1f);
        StabPlayer = SaveScript.Stab;
        BloodSplat = SaveScript.Blood;
        BloodSplat.gameObject.SetActive(false);
        CanDamage = true;
    }
}
