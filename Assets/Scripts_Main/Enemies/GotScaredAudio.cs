using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GotScaredAudio : MonoBehaviour
{
    private AudioSource AudioPlayer;
    [SerializeField] AudioClip Scream1;
    [SerializeField] AudioClip Scream2;
    [SerializeField] AudioClip Scream3;
    void Start()
    {
        AudioPlayer = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (SaveScript.GotScared)
        {
            SaveScript.GotScared = false;
            StartCoroutine(Loop());
        }
    }

    void SetScreamClip()
    {
        int screamNo = Random.Range(1, 4);
        if (screamNo == 1)
        {
            AudioPlayer.clip = Scream1;
        }
        if (screamNo == 2)
        {
            AudioPlayer.clip = Scream2;
        }
        if (screamNo == 3)
        {
            AudioPlayer.clip = Scream3;
        }
    }

    IEnumerator Loop()
    {
        for (var i = 0; i < 3; i++)
        {
            SetScreamClip();
            AudioPlayer.Play();
            yield return new WaitForSeconds(AudioPlayer.clip.length);
        }
    }
}
