using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InspectionObj : MonoBehaviour
{
    public GameObject[] inspectionObjects;
    public GameObject inspectionSystem;
    public GameObject FPSController;
    private int currIndex;

    public List<KeyValuePair<Vector3, Quaternion>> firstRotationsAndPositions;
    public UIMessageSystem MessageSystem;

    public void TurnOnInspection(int index)
    {
        currIndex = index;
        inspectionObjects[index].SetActive(true);
    }

    public void TurnOffInspection()
    {
        inspectionObjects[currIndex].SetActive(false);
    }

    void Start()
    {
        firstRotationsAndPositions = new List<KeyValuePair<Vector3, Quaternion>>();
        for(int i=0; i<inspectionObjects.Length; i++)
        {
            Vector3 firstPosition = inspectionObjects[i].transform.position;
            Quaternion firstRotation = inspectionObjects[i].transform.rotation;
            KeyValuePair<Vector3, Quaternion> firstRotationAndPosition = new KeyValuePair<Vector3, Quaternion>(firstPosition, firstRotation);
            firstRotationsAndPositions.Add(firstRotationAndPosition);
        }
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            Vector3 firstPosition = firstRotationsAndPositions[currIndex].Key;
            Quaternion firstRotation = firstRotationsAndPositions[currIndex].Value;
            inspectionObjects[currIndex].gameObject.transform.SetPositionAndRotation(firstPosition, firstRotation);

            inspectionSystem.SetActive(false);
            TurnOffInspection();
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            Time.timeScale = 1;
            FPSController.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;
            
            MessageSystem.EnableCrosshair();
            MessageSystem.EnableSanity();
        }
    }
}
