using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CantFitThroughHole : MonoBehaviour
{
    private Camera mainCamera;
    public UIMessageSystem MessageSystem;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!SaveScript.StartingAnimationFinished) return;

        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 3f))
        {
            if (hit.transform.gameObject.GetInstanceID() != gameObject.GetInstanceID()) return;

            MessageSystem.DisableAll();
            MessageSystem.EnableSanity();
            MessageSystem.EnableHoleWCMessage();
        }
        else
        {
            MessageSystem.EnableCrosshair();
            MessageSystem.DisableHoleWCMessage();
        }
    }

    private void OnMouseExit()
    {
        MessageSystem.EnableCrosshair();
        MessageSystem.DisableHoleWCMessage();
    }
}
