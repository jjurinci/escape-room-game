using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneShotSound : MonoBehaviour
{
    private AudioSource OneShot;
    private Collider Col;
    [SerializeField] bool OneTime = false;
    [SerializeField] float PauseTime = 5.0f;

    void Start()
    {
        OneShot = GetComponent<AudioSource>();
        Col = GetComponent<Collider>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            SaveScript.GotScared = true;
            if (!gameObject.CompareTag("OneshotAlways"))
            {
                SaveScript.OneshotTriggered++;
            }
            OneShot.Play();
            Col.enabled = false;

            if (OneTime)
            {
                Destroy(gameObject, PauseTime);
            }
            else
            {
                StartCoroutine(Reset());
            }
        }
    }

    IEnumerator Reset()
    {
        yield return new WaitForSeconds(PauseTime);
        Col.enabled = true;
    }
}
