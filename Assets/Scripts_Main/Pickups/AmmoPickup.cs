using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPickup : MonoBehaviour
{
    [SerializeField] int AmmoCount;
    void Start()
    {
        StartCoroutine(CheckAmmo());
    }

    IEnumerator CheckAmmo()
    {
        yield return new WaitForSeconds(1);
        if (AmmoCount > SaveScript.AmmoLeft)
        {
            Destroy(gameObject);
        }
    }
}
