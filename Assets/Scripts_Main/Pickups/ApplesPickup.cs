using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplesPickup : MonoBehaviour
{
    [SerializeField] int AppleCount;
    void Start()
    {
        StartCoroutine(CheckApples());
    }

    IEnumerator CheckApples()
    {
        yield return new WaitForSeconds(1);
        if (AppleCount > SaveScript.ApplesLeft)
        {
            Destroy(gameObject);
        }
    }
}
