using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterPositionSave : MonoBehaviour
{
    [SerializeField] CharacterController Player;

    void Start()
    {
        StartCoroutine(SetPosition());
    }

    IEnumerator SetPosition()
    {
        yield return new WaitForSeconds(0.1f);
        if (PlayerPrefs.GetFloat("PlayerPositionX") > 0.0f)
        {
            if (!SaveScript.StartedNewGame)
            {
                Player.enabled = false;
                Player.transform.position = new Vector3(
                    PlayerPrefs.GetFloat("PlayerPositionX"),
                    PlayerPrefs.GetFloat("PlayerPositionY"),
                    PlayerPrefs.GetFloat("PlayerPositionZ")
                );
                Player.enabled = true;
            }
        }
    }
}
