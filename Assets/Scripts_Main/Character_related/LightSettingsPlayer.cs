using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class LightSettingsPlayer : MonoBehaviour
{
    [SerializeField] PostProcessVolume Volume;
    [SerializeField] PostProcessProfile Standard;
    [SerializeField] PostProcessProfile NightVision;
    [SerializeField] GameObject NightVisionOverlay;
    [SerializeField] GameObject Flashlight;
    [SerializeField] GameObject EnemyFlashlight;
    public Light FLLight;

    private bool NightVisionActive;
    private bool FlashlightActive;

    void Start()
    {
        NightVisionOverlay.gameObject.SetActive(false);
        Flashlight.gameObject.SetActive(false);
        EnemyFlashlight.gameObject.SetActive(false);
    }

    void Update()
    {
        if (SaveScript.BatteryPower > 0.0f)
        {
            if (SaveScript.NVGoggles)
            {
                if (Input.GetKeyDown(KeyCode.N))
                {
                    if (NightVisionActive)
                    {
                        Volume.profile = Standard;
                        NightVisionActive = false;
                        NightVisionOverlay.gameObject.SetActive(false);
                        SaveScript.NVLightOn = false;
                    }
                    else
                    {
                        Volume.profile = NightVision;
                        NightVisionActive = true;
                        NightVisionOverlay.gameObject.SetActive(true);
                        SaveScript.NVLightOn = true;
                    }
                }
            }

            if (SaveScript.FlashLight)
            {   
                if (Input.GetKeyDown(KeyCode.F))
                {
                    if (FlashlightActive)
                    {
                        EnemyFlashlight.gameObject.SetActive(false);
                        Flashlight.gameObject.SetActive(false);
                        FlashlightActive = false;
                        SaveScript.FlashLightOn = false;
                    }
                    else
                    {
                        EnemyFlashlight.gameObject.SetActive(true);
                        Flashlight.gameObject.SetActive(true);
                        FlashlightActive = true;
                        SaveScript.FlashLightOn = true;
                    }
                }
            }

            if (SaveScript.BatteryPower < 0.55f)
            {
                FLLight.range = SaveScript.BatteryPower * 100;
            }
            else
            {
                FLLight.range = 55f;
            }
        }

        if (SaveScript.BatteryPower <= 0.0f)
        {
            Volume.profile = Standard;
            NightVisionActive = false;
            NightVisionOverlay.gameObject.SetActive(false);
            SaveScript.NVLightOn = false;
            EnemyFlashlight.gameObject.SetActive(false);
            Flashlight.gameObject.SetActive(false);
            FlashlightActive = false;
            SaveScript.FlashLightOn = false;
        }
    }
}
