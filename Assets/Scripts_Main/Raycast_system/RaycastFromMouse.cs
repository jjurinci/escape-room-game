using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastFromMouse : MonoBehaviour
{
    public UIMessageSystem messageSystem;
    private Camera _mainCamera;
    private int _inspectableLayer;
    private int _pickupsLayer;
    private int _smallDoorsLayer;
    private int _keypadLayer;
    private int _breakPlaneLayer;
    private int _crouchAreaLayer;
    private int _electricityBoxLayer;
    private int _electricityDoorLayer;

    private GameObject _lastHitObject = null;

    // Start is called before the first frame update
    void Start()
    {
        _mainCamera = Camera.main.GetComponent<Camera>();
        _inspectableLayer = LayerMask.GetMask("Inspectable");
        _pickupsLayer = LayerMask.GetMask("Pickups");
        _smallDoorsLayer = LayerMask.GetMask("SmallDoors");
        _keypadLayer = LayerMask.GetMask("Keypads");
        _breakPlaneLayer = LayerMask.GetMask("BreakPlane");
        _crouchAreaLayer = LayerMask.GetMask("CrouchArea");
        _electricityBoxLayer = LayerMask.GetMask("ElectricityBox");
        _electricityDoorLayer = LayerMask.GetMask("ElectricityDoor");
    }

    // Update is called once per frame
    void Update()
    {
        if (!SaveScript.StartingAnimationFinished) return;
        if (Time.timeScale == 0) return;

        Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if(Physics.Raycast(ray, out hit, 3f))
        {
            // Remove outline from last hit object
            if (_lastHitObject != null && gameObject.GetInstanceID() != _lastHitObject.GetInstanceID())
            {
                Outline outline = _lastHitObject.GetComponent<Outline>();
                if (outline) outline.OutlineWidth = 0f;
            }

            GameObject hitObject = hit.collider.gameObject;
            int layer = 1 << hit.transform.gameObject.layer;

            if (layer == _smallDoorsLayer) hitObject.GetComponent<OpenCloseSmallDoors>().HandleSmallDoors();
            else if (layer == _pickupsLayer) hitObject.GetComponent<Pickups>().HandlePickup(hit);
            else if (layer == _inspectableLayer) hitObject.GetComponent<HovObject>().HandleInspect();
            else if (layer == _keypadLayer) hitObject.GetComponent<Keypad>().HandleKeypad(hit);
            else if (layer == _breakPlaneLayer) hitObject.GetComponent<DamageMetalProblemOne>().HandleBreakPlane();
            else if (layer == _crouchAreaLayer) hitObject.GetComponent<CrouchMessage>().HandleCrouchMessage();
            else if (layer == _electricityDoorLayer) hitObject.GetComponent<ElectricityDoor>().HandleElectricityDoor();
            else if (layer == _electricityBoxLayer) hitObject.GetComponent<ElectricityExplode>().HandleElectricityExplode();
            _lastHitObject = hitObject;
        }
        else
        {
            if (_lastHitObject != null && gameObject.GetInstanceID() != _lastHitObject.GetInstanceID())
            {
                Outline outline = _lastHitObject.GetComponent<Outline>();
                if (outline) outline.OutlineWidth = 0f;
                messageSystem.DisableAll();
                messageSystem.EnableSanity();
                messageSystem.EnableCrosshair();
            }
        }
    }
}
