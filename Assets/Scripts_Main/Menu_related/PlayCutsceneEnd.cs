using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayCutsceneEnd : MonoBehaviour
{
    public VideoPlayer video;
    public GameObject rawImage;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LoadVideo());
    }

    IEnumerator LoadVideo()
    {
        rawImage.SetActive(true);
        RawImage image = rawImage.GetComponent<RawImage>();

        video.Prepare();
        while (!video.isPrepared)
        {
            yield return new WaitForSeconds(1);
        }
        image.texture = video.texture;

        video.Play();
        yield return new WaitForSeconds(46);
        rawImage.SetActive(false);

        SceneManager.LoadScene(1);
    }
}
