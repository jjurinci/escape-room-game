using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyRiddleObjects : MonoBehaviour
{
    [SerializeField] GameObject SheetRack;
    [SerializeField] GameObject Plane;
    [SerializeField] GameObject Lighter;
    [SerializeField] GameObject GasCylinder;
    [SerializeField] GameObject Door;
    [SerializeField] GameObject WaterBucket;
    [SerializeField] GameObject OfficeDoor;
    [SerializeField] GameObject Flashlight;
    [SerializeField] GameObject NVGoggles;
    [SerializeField] List<GameObject> Keys;
    [SerializeField] GameObject BedroomDoorOne;
    [SerializeField] GameObject BedroomDoorTwo;
    [SerializeField] GameObject ElectricityBoxDoor;

    void Start()
    {
        StartCoroutine(CheckElements());
    }

    IEnumerator CheckElements()
    {
        yield return new WaitForSeconds(1);
        if (SaveScript.PickedUpLighter)
        {
            Destroy(Lighter.gameObject);
        }
        if (SaveScript.SecretTunnelDestroyed)
        {
            Destroy(Plane.gameObject);
            Destroy(SheetRack.gameObject);
        }
        if (SaveScript.SecretTunnelDestroyed)
        {
            Destroy(Plane.gameObject);
            Destroy(SheetRack.gameObject);
        }
        if (SaveScript.GasCylinderExploded)
        {
            Destroy(GasCylinder.gameObject);
            Destroy(Door.gameObject);
        }
        if (SaveScript.PickedUpWaterBucket)
        {
            Destroy(WaterBucket.gameObject);
        }
        if (SaveScript.FlashLight)
        {
            Destroy(Flashlight.gameObject);
        }
        for (var i = 0; i < Keys.Count; i++)
        {
            if (SaveScript.Keys[i].GetActive())
            {
                Destroy(Keys[i].gameObject);
            }
        }
        if (SaveScript.OfficeDoorDestroyed)
        {
            Destroy(OfficeDoor.gameObject);
        }
        if (SaveScript.NVGoggles)
        {
            Destroy(NVGoggles.gameObject);
        }
        if (SaveScript.OpenedBedroomDoors)
        {
            Destroy(BedroomDoorOne);
            Destroy(BedroomDoorTwo);
        }
        if (SaveScript.ElectricityBoxDoorOpened)
        {
            Destroy(ElectricityBoxDoor);
        }
    }
}
