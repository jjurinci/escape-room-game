using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class OptionsMenu : MonoBehaviour
{
    [SerializeField] GameObject VisualsPanel;
    [SerializeField] GameObject SoundsPanel;
    [SerializeField] GameObject ControlsPanel;
    [SerializeField] GameObject DifficultyPanel;
    [SerializeField] GameObject SavePanel;
    [SerializeField] GameObject BackToMenuPanel;

    [SerializeField] GameObject FogParticles;
    [SerializeField] PostProcessLayer Layer;

    public AudioMixer AmbienceMixer;
    public AudioMixer SFXMixer;
    public Slider AmbienceVolume;
    public Slider SFXVolume;
    public Slider BrightnessIntensity;
    public Toggle FogToggle;
    public Toggle AAToggle;
    public Toggle FXAAToggle;
    public Toggle SMAAToggle;
    public Toggle TAAToggle;

    public bool FogOn = true;

    void Start()
    {
        Cursor.visible = true;
        Time.timeScale = 0.0f;
        VisualsPanel.gameObject.SetActive(true);
        SoundsPanel.gameObject.SetActive(false);
        ControlsPanel.gameObject.SetActive(false);
        DifficultyPanel.gameObject.SetActive(false);
        SavePanel.gameObject.SetActive(false);
        BackToMenuPanel.gameObject.SetActive(false);
    }

    void Update()
    {
        Cursor.visible = true;
        Time.timeScale = 0.0f;
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene(1);
    }

    public void AdjustAmbienceVolume()
    {
        AmbienceMixer.SetFloat("Volume", AmbienceVolume.value);
    }

    public void AdjustSFXVolume()
    {
        SFXMixer.SetFloat("Volume", SFXVolume.value);
    }

    public void AdjustBrightness()
    {
        RenderSettings.ambientIntensity = BrightnessIntensity.value;
        RenderSettings.reflectionIntensity = BrightnessIntensity.value;
        DynamicGI.UpdateEnvironment();
    }

    public void Easy()
    {
        SaveScript.EnemyWeaponDamage = 10;
        SaveScript.DifficultyChanged = true;
    }

    public void Medium()
    {
        SaveScript.EnemyWeaponDamage = 30;
        SaveScript.DifficultyChanged = true;
    }

    public void Hard()
    {
        SaveScript.EnemyWeaponDamage = 50;
        SaveScript.DifficultyChanged = true;
    }

    public void ToggleFog()
    {
        if (FogToggle.isOn)
        {
            Layer.fog.enabled = true;
            FogParticles.gameObject.SetActive(true);
        }
        else
        {
            Layer.fog.enabled = false;
            FogParticles.gameObject.SetActive(false);
        }
    }

    public void AntiAliasingOff()
    {
        if (AAToggle.isOn)
        {
            Layer.antialiasingMode = PostProcessLayer.Antialiasing.None;
            AAToggle.enabled = false;
            FXAAToggle.isOn = false;
            FXAAToggle.enabled = true;
            SMAAToggle.isOn = false;
            SMAAToggle.enabled = true;
            TAAToggle.isOn = false;
            TAAToggle.enabled = true;
        }
    }

    public void AntiAliasingFXAA()
    {
        if (FXAAToggle.isOn)
        {
            Layer.antialiasingMode = PostProcessLayer.Antialiasing.FastApproximateAntialiasing;
            FXAAToggle.enabled = false;
            AAToggle.isOn = false;
            AAToggle.enabled = true;
            SMAAToggle.isOn = false;
            SMAAToggle.enabled = true;
            TAAToggle.isOn = false;
            TAAToggle.enabled = true;
        }
    }

    public void AntiAliasingSMAA()
    {
        if (SMAAToggle.isOn)
        {
            Layer.antialiasingMode = PostProcessLayer.Antialiasing.SubpixelMorphologicalAntialiasing;
            SMAAToggle.enabled = false;
            AAToggle.isOn = false;
            AAToggle.enabled = true;
            FXAAToggle.isOn = false;
            FXAAToggle.enabled = true;
            TAAToggle.isOn = false;
            TAAToggle.enabled = true;
        }
    }

    public void AntiAliasingTAA()
    {
        if (TAAToggle.isOn)
        {
            Layer.antialiasingMode = PostProcessLayer.Antialiasing.TemporalAntialiasing;
            TAAToggle.enabled = false;
            AAToggle.isOn = false;
            AAToggle.enabled = true;
            FXAAToggle.isOn = false;
            FXAAToggle.enabled = true;
            SMAAToggle.isOn = false;
            SMAAToggle.enabled = true;
        }
    }

    public void Visuals()
    {
        VisualsPanel.gameObject.SetActive(true);
        SoundsPanel.gameObject.SetActive(false);
        ControlsPanel.gameObject.SetActive(false);
        DifficultyPanel.gameObject.SetActive(false);
        SavePanel.gameObject.SetActive(false);
        BackToMenuPanel.gameObject.SetActive(false);
    }

    public void Sounds()
    {
        VisualsPanel.gameObject.SetActive(false);
        SoundsPanel.gameObject.SetActive(true);
        ControlsPanel.gameObject.SetActive(false);
        DifficultyPanel.gameObject.SetActive(false);
        SavePanel.gameObject.SetActive(false);
        BackToMenuPanel.gameObject.SetActive(false);
    }

    public void Controls()
    {
        VisualsPanel.gameObject.SetActive(false);
        SoundsPanel.gameObject.SetActive(false);
        ControlsPanel.gameObject.SetActive(true);
        DifficultyPanel.gameObject.SetActive(false);
        SavePanel.gameObject.SetActive(false);
        BackToMenuPanel.gameObject.SetActive(false);
    }

    public void Difficulty()
    {
        VisualsPanel.gameObject.SetActive(false);
        SoundsPanel.gameObject.SetActive(false);
        ControlsPanel.gameObject.SetActive(false);
        DifficultyPanel.gameObject.SetActive(true);
        SavePanel.gameObject.SetActive(false);
        BackToMenuPanel.gameObject.SetActive(false);
    }

    public void Save()
    {
        VisualsPanel.gameObject.SetActive(false);
        SoundsPanel.gameObject.SetActive(false);
        ControlsPanel.gameObject.SetActive(false);
        DifficultyPanel.gameObject.SetActive(false);
        SavePanel.gameObject.SetActive(true);
        BackToMenuPanel.gameObject.SetActive(false);
    }

    public void BackToMenu()
    {
        VisualsPanel.gameObject.SetActive(false);
        SoundsPanel.gameObject.SetActive(false);
        ControlsPanel.gameObject.SetActive(false);
        DifficultyPanel.gameObject.SetActive(false);
        SavePanel.gameObject.SetActive(false);
        BackToMenuPanel.gameObject.SetActive(true);
    }
}
