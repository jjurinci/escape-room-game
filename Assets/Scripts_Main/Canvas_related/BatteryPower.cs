using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BatteryPower : MonoBehaviour
{
    [SerializeField] GameObject BatteryUI;
    [SerializeField] float DrainTime = 60.0f;
    [SerializeField] float Power;
    private Image BatteryUIimage;
    private void Start()
    {
        BatteryUIimage = BatteryUI.GetComponent<Image>();
    }

    void Update()
    {
        if(SaveScript.FlashLight && !BatteryUI.activeSelf)
        {
            BatteryUI.SetActive(true);
        }

        if (SaveScript.BatteryRefill)
        {
            BatteryUIimage.fillAmount = 1.0f;
            SaveScript.BatteryRefill = false;
        }
        if (SaveScript.FlashLightOn || SaveScript.NVLightOn)
        {
            BatteryUIimage.fillAmount -= 1.0f / DrainTime * Time.deltaTime;
            Power = BatteryUIimage.fillAmount;
            SaveScript.BatteryPower = Power;
        }
    }
}
