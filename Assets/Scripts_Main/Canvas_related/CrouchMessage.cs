using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrouchMessage : MonoBehaviour
{
    private Camera _mainCamera;
    public UIMessageSystem MessageSystem;

    private void Start()
    {
        _mainCamera = Camera.main.GetComponent<Camera>();
    }

    void Update()
    {
    }

    public void HandleCrouchMessage()
    {
        if (SaveScript.InInspectMode || 
            SaveScript.InInventoryMode || 
            SaveScript.InOptionsMode || 
            SaveScript.InKeypadMode) return;

        MessageSystem.DisableAll();
        MessageSystem.EnableSanity();

        if (!SaveScript.SecretTunnelDestroyed) MessageSystem.EnableCouldBreakMessage();
        else MessageSystem.EnableCrouchMessage();
    }

    private void OnMouseExit()
    {
        if (Time.deltaTime == 0) return;

        MessageSystem.EnableCrosshair();
        MessageSystem.DisableCrouchMessage();
        MessageSystem.DisableCouldBreakMessage();
    }
}
