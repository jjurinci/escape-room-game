using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunUI : MonoBehaviour
{
    [SerializeField] Text BulletUI;

    void Start()
    {
        BulletUI.text = SaveScript.Bullets.ToString();
    }

    void Update()
    {
        BulletUI.text = SaveScript.Bullets.ToString();
        if (SaveScript.InInventoryMode || SaveScript.InOptionsMode || SaveScript.InInspectMode)
        {
            return;
        }
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (SaveScript.Bullets > 0)
            {
                SaveScript.Bullets--;
            }
        }
    }
}
