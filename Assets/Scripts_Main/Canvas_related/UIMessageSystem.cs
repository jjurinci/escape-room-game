using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMessageSystem : MonoBehaviour
{
    public GameObject SanityMessage;
    public GameObject Crosshair;
    public GameObject PickupMessage;
    public GameObject DoorMessage;
    public GameObject InspectMessage;
    public GameObject MoveMessage;
    public GameObject ThrowMessage;
    public GameObject ElectricityMessage_NeedKey;
    public GameObject ElectricityMessage_NeedWater;
    public GameObject ElectricityMessage_NeedFillBucket;
    public GameObject ElectricityMessage_NeedSplash;
    public GameObject BucketMessage_Pickup;
    public GameObject BucketMessage_FillWater;
    public GameObject HoleWCMessage;
    public GameObject CouldBreakMessage;
    public GameObject CrouchMessage;
    public GameObject OpenInventoryMessage;
    public GameObject KeypadMessage;
    public GameObject ExplosionMessageLoose;
    public GameObject ExplosionMessageExplode;

    public GameObject _flashlightMessage;
    public GameObject _nightVisionMessage;

    public void DisableAll()
    {
        Crosshair.SetActive(false);
        PickupMessage.SetActive(false);
        DoorMessage.SetActive(false);
        InspectMessage.SetActive(false);
        MoveMessage.SetActive(false);
        ThrowMessage.SetActive(false);
        SanityMessage.SetActive(false);
        ElectricityMessage_NeedKey.SetActive(false);
        ElectricityMessage_NeedWater.SetActive(false);
        ElectricityMessage_NeedFillBucket.SetActive(false);
        ElectricityMessage_NeedSplash.SetActive(false);
        BucketMessage_Pickup.SetActive(false);
        BucketMessage_FillWater.SetActive(false);
        HoleWCMessage.SetActive(false);
        CouldBreakMessage.SetActive(false);
        CrouchMessage.SetActive(false);
        OpenInventoryMessage.SetActive(false);
        KeypadMessage.SetActive(false);
    }
    public void DisableCrosshair()
    {
        Crosshair.SetActive(false);
    }

    public void DisablePickupMessage()
    {
        PickupMessage.SetActive(false);
    }

    public void DisableDoorMessage()
    {
        DoorMessage.SetActive(false);
    }

    public void DisableInspectMessage()
    {
        InspectMessage.SetActive(false);
    }

    public void DisableMoveMessage()
    {
        MoveMessage.SetActive(false);
    }

    public void DisableThrowMessage()
    {
        ThrowMessage.SetActive(false);
    }

    public void DisableSanityMessage()
    {
        SanityMessage.SetActive(false);
    }

    public void EnablePickupMessage()
    {
        PickupMessage.SetActive(true);
    }

    public void EnableMoveMessage()
    {
        MoveMessage.SetActive(true);
    }
    public void EnableThrowMessage()
    {
        ThrowMessage.SetActive(true);
    }

    public void EnableInspectMessage()
    {
        InspectMessage.SetActive(true);
    }

    public void EnableCrosshair()
    {
        Crosshair.SetActive(true);
    }
    public void EnableSanity()
    {
        SanityMessage.SetActive(true);
    }
    public void EnableOpenDoorMessage()
    {
        DoorMessage.GetComponentInChildren<Text>().text = "Press E to open.";
        DoorMessage.SetActive(true);
    }
    public void EnableCloseDoorMessage()
    {
        DoorMessage.GetComponentInChildren<Text>().text = "Press E to close.";
        DoorMessage.SetActive(true);
    }

    public void EnableLockedDoorMessage()
    {
        DoorMessage.GetComponentInChildren<Text>().text = "The door is locked.";
        DoorMessage.SetActive(true);
    }

    public void EnableElectricityMessage_NeedKey()
    {
        ElectricityMessage_NeedKey.SetActive(true);
    }
    public void EnableElectricityMessage_NeedWater()
    {
        ElectricityMessage_NeedWater.SetActive(true);
    }
    public void EnableElectricityMessage_NeedFillBucket()
    {
        ElectricityMessage_NeedFillBucket.SetActive(true);
    }
    public void EnableElectricityMessage_NeedSplash()
    {
        ElectricityMessage_NeedSplash.SetActive(true);
    }
    public void EnableBucketMessage_Pickup()
    {
        BucketMessage_Pickup.SetActive(true);
    }
    public void EnableBucketMessage_FillWater()
    {
        BucketMessage_FillWater.SetActive(true);
    }
    public void EnableHoleWCMessage()
    {
        HoleWCMessage.SetActive(true);
    }
    public void EnableCouldBreakMessage()
    {
        CouldBreakMessage.SetActive(true);
    }
    public void EnableCrouchMessage()
    {
        CrouchMessage.SetActive(true);
    }
    public void EnableOpenInventoryMessage()
    {
        OpenInventoryMessage.SetActive(true);
    }
    public void EnableKeypadMessage()
    {
        KeypadMessage.SetActive(true);
    }

    public void EnableExplosionMessageLoose()
    {
        ExplosionMessageLoose.SetActive(true);
    }

    public void EnableExplosionMessageExplode()
    {
        ExplosionMessageExplode.SetActive(true);
    }

    public void DisableElectricityMessage_NeedKey()
    {
        ElectricityMessage_NeedKey.SetActive(false);
    }
    public void DisableElectricityMessage_NeedWater()
    {
        ElectricityMessage_NeedWater.SetActive(false);
    }
    public void DisableElectricityMessage_NeedFillBucket()
    {
        ElectricityMessage_NeedFillBucket.SetActive(false);
    }
    public void DisableElectricityMessage_NeedSplash()
    {
        ElectricityMessage_NeedSplash.SetActive(false);
    }
    public void DisableBucketMessage_Pickup()
    {
        BucketMessage_Pickup.SetActive(false);
    }
    public void DisableBucketMessage_FillWater()
    {
        BucketMessage_FillWater.SetActive(false);
    }
    public void DisableHoleWCMessage()
    {
        HoleWCMessage.SetActive(false);
    }
    public void DisableCouldBreakMessage()
    {
        CouldBreakMessage.SetActive(false);
    }
    public void DisableCrouchMessage()
    {
        CrouchMessage.SetActive(false);
    }
    public void DisableOpenInventoryMessage()
    {
        OpenInventoryMessage.SetActive(false);
    }

    public void DisableKeypadMessage()
    {
        KeypadMessage.SetActive(false);
    }

    public void DisableExplosionMessageLoose()
    {
        ExplosionMessageLoose.SetActive(false);
    }

    public void DisableExplosionMessageExplode()
    {
        ExplosionMessageExplode.SetActive(false);
    }

    public void EnableNightVisionMessage()
    {
        StartCoroutine("NightVisionCoroutine");
    }

    public void EnableFlashlightMessage()
    {
        StartCoroutine("FlashlightCoroutine");
    }

    public IEnumerator NightVisionCoroutine()
    {
        _nightVisionMessage.gameObject.SetActive(true);
        yield return new WaitForSeconds(5);
        _nightVisionMessage.gameObject.SetActive(false);
    }

    public IEnumerator FlashlightCoroutine()
    {
        _flashlightMessage.gameObject.SetActive(true);
        yield return new WaitForSecondsRealtime(5);
        _flashlightMessage.gameObject.SetActive(false);
    }
}
