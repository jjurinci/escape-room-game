using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlsHelper : MonoBehaviour
{
    public GameObject ControlText;
    bool timeStarted = false;

    void Update()
    {
        if (!SaveScript.StartingAnimationFinished || timeStarted) return;

        ControlText.SetActive(true);
        timeStarted = true;
        StartCoroutine(Wait20Seconds());
    }

    IEnumerator Wait20Seconds()
    {
        yield return new WaitForSeconds(15);
        ControlText.SetActive(false);
    }
}
