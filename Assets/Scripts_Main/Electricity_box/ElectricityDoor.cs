using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricityDoor : MonoBehaviour
{
    private Animator animator;
    private Outline outlineObj;
    private Camera mainCamera;
    public UIMessageSystem MessageSystem;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        outlineObj = GetComponent<Outline>();
        mainCamera = Camera.main.GetComponent<Camera>();
    }

    public void HandleElectricityDoor()
    {
        if (gameObject.tag == "Outlineable") outlineObj.OutlineWidth = 10f;

        MessageSystem.DisableAll();
        MessageSystem.EnableSanity();

        if (SaveScript.PickedUpElectricityKey && SaveScript.ElectricityBoxDoorOpened)
            MessageSystem.EnableCloseDoorMessage();

        else if (SaveScript.PickedUpElectricityKey && !SaveScript.ElectricityBoxDoorOpened)
            MessageSystem.EnableOpenDoorMessage();

        else if (!SaveScript.PickedUpElectricityKey)
            MessageSystem.EnableElectricityMessage_NeedKey();

        if (Input.GetKeyDown(KeyCode.E) && SaveScript.Keys[1].GetActive())
        {
            if (!SaveScript.ElectricityBoxDoorOpened)
            {
                animator.SetTrigger("Open");
                SaveScript.ElectricityBoxDoorOpened = true;
            }
            else if (SaveScript.ElectricityBoxDoorOpened)
            {
                animator.SetTrigger("Close");
                SaveScript.ElectricityBoxDoorOpened = false;
            }
        }

    }

    private void OnMouseExit()
    {
        if (Time.deltaTime == 0) return;

        outlineObj.OutlineWidth = 0f;
        MessageSystem.EnableCrosshair();
        MessageSystem.DisableElectricityMessage_NeedKey();
        MessageSystem.DisableDoorMessage();
    }
}
