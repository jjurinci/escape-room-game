using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinkFillWater : MonoBehaviour
{
    public AudioSource waterFillAudio;
    public Outline outlineObjSink;
    public Outline outlineObjTap;
    private Camera mainCamera;

    public UIMessageSystem MessageSystem;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!SaveScript.StartingAnimationFinished) return;
        if (!SaveScript.PickedUpWaterBucket) return;
        if (SaveScript.FilledBucketWithWater) return;

        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 3f))
        {
            if (hit.transform.gameObject.GetInstanceID() != gameObject.GetInstanceID()) return;
            if (gameObject.tag == "Outlineable")
            {
                outlineObjSink.OutlineWidth = 10f;
                outlineObjTap.OutlineWidth = 10f;
            }

            MessageSystem.DisableAll();
            MessageSystem.EnableSanity();
            MessageSystem.EnableBucketMessage_FillWater();

            if (Input.GetKeyDown(KeyCode.E))
            {
                waterFillAudio.Play();
                SaveScript.FilledBucketWithWater = true;
                MessageSystem.EnableCrosshair();
                MessageSystem.DisableBucketMessage_FillWater();
            }
        }
        else
        {
            MessageSystem.EnableCrosshair();
            MessageSystem.DisableBucketMessage_FillWater();
            if (outlineObjSink && outlineObjSink.OutlineWidth != 0) outlineObjSink.OutlineWidth = 0f;
            if (outlineObjTap && outlineObjTap.OutlineWidth != 0) outlineObjTap.OutlineWidth = 0f;
        }
    }

    private void OnMouseExit()
    {
        MessageSystem.EnableCrosshair();
        MessageSystem.DisableBucketMessage_FillWater();
        outlineObjSink.OutlineWidth = 0f;
        outlineObjTap.OutlineWidth = 0f;
    }
}
