using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour
{

    public GameObject smoke;

    public AudioSource electricity;

    public AudioSource power_off;

    public bool isTrigger = false;

    public Collider collider;

    public GameObject lightning;

    void Start()
    {
        smoke = GameObject.Find("WFX_SmokeGrenade AlphaBlend Gray");
        smoke.SetActive(false);
        lightning = GameObject.Find("CFX_ElectricityBall");
        lightning.SetActive(false);
        collider = GetComponent<Collider>();
    }

    private void OnTriggerEnter(Collider other) {
        isTrigger = true;
    }

    private void OnTriggerExit(Collider other) {
        isTrigger = false;
    }

    private void Update() {
        if(isTrigger){
            if (Input.GetKeyDown(KeyCode.E))
                {
                electricity.Play();
                lightning.SetActive(true);
                power_off.Play();
                smoke.SetActive(true);
                collider.enabled = false;
                isTrigger = false;
                }  
        }
    }
}
