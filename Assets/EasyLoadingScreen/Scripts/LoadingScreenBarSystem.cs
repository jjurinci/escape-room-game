﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class LoadingScreenBarSystem : MonoBehaviour {

    public GameObject bar;
    public Text loadingText;
    public bool backGroundImageAndLoop;
    public bool skipButtonPressed = false;
    public float LoopTime;
    public GameObject[] backgroundImages;
    [Range(0,1f)]public float vignetteEfectVolue; // Must be a value between 0 and 1
    AsyncOperation async;
    Image vignetteEfect;
    public GameObject MenuScreen;
    public GameObject LoadingScreen;
    public GameObject CutsceneIntro;
    public GameObject rawImage;
    public AudioSource startingAudio;
    public GameObject skipText;
    private VideoPlayer video;
    private RawImage image;

    public void loadingScreen (int sceneNo)
    {
        LoadingScreen.gameObject.SetActive(true);
        MenuScreen.gameObject.SetActive(false);
        this.gameObject.SetActive(true);
        StartCoroutine(Loading(sceneNo));
    }

    public void SwitchOff()
    {
        LoadingScreen.gameObject.SetActive(true);
        MenuScreen.gameObject.SetActive(false);
    }

    public void PlayVideo()
    {
        StartCoroutine(LoadVideo());
    }

    IEnumerator LoadVideo()
    {
        rawImage.SetActive(true);
        video = CutsceneIntro.GetComponent<VideoPlayer>();
        image = rawImage.GetComponent<RawImage>();
        startingAudio.mute = true;
        video.Prepare();
        while(!video.isPrepared)
        {
            yield return new WaitForSeconds(1);
        }
        image.texture = video.texture;
        
        video.Play();

        skipText.SetActive(true);

        yield return new WaitForSeconds(72);
        rawImage.SetActive(false);
        skipText.SetActive(false);

        if (skipButtonPressed) yield break;

        SwitchOff();
        loadingScreen(2);
        NewGame();
    }

    public void NewGame()
    {
        SaveScript.NewGame = true;
    }

    // Used to try. Delete the comment lines (25 and 36)
    /*
    public void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            bar.transform.localScale += new Vector3(0.001f,0,0);

            if (loadingText != null)
                loadingText.text = "%" + (100 * bar.transform.localScale.x).ToString("####");
        }
    }
    */

    private void Start()
    {
        vignetteEfect = transform.Find("VignetteEfect").GetComponent<Image>();
        vignetteEfect.color = new Color(vignetteEfect.color.r,vignetteEfect.color.g,vignetteEfect.color.b,vignetteEfectVolue);
        MenuScreen.gameObject.SetActive(true);
        //if (backGroundImageAndLoop)
            //StartCoroutine(transitionImage());
    }

    private void Update()
    {
        if (Cursor.lockState == CursorLockMode.Locked)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.Confined;
        }

        if (Input.GetKeyDown(KeyCode.S) && video.isPlaying)
        {
            skipButtonPressed = true;
            video.Stop();
            rawImage.SetActive(false);
            skipText.SetActive(false);
            SwitchOff();
            loadingScreen(2);
            NewGame();
        }
    }




    // The pictures change according to the time of
    IEnumerator transitionImage ()
    {
        for (int i = 0; i < backgroundImages.Length; i++)
        {
            yield return new WaitForSeconds(LoopTime);
            for (int j = 0; j < backgroundImages.Length; j++)
                backgroundImages[j].SetActive(false);
            backgroundImages[i].SetActive(true);           
        }
    }

    // Activate the scene 
    IEnumerator Loading (int sceneNo)
    {
        async = SceneManager.LoadSceneAsync(sceneNo);
        async.allowSceneActivation = false;

        // Continue until the installation is completed
        while (async.isDone == false)
        {
            bar.transform.localScale = new Vector3(async.progress,0.9f,1);

            if (loadingText != null)
                loadingText.text = "%" + (100 * bar.transform.localScale.x).ToString("####");

            if (async.progress == 0.9f)
            {
                bar.transform.localScale = new Vector3(1, 0.9f, 1);
                async.allowSceneActivation = true;
            }
            yield return null;
        }
    }



}
