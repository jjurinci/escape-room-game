using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Keypad : MonoBehaviour
{
    [SerializeField] public GameObject Keypad2D;
    [SerializeField] public Text FeedbackMessage;
    [SerializeField] public Text CodeMessage;
    [SerializeField] public List<Button> UIButtons;
    [SerializeField] public GameObject FPSArms;
    [SerializeField] public GameObject FPSController;
    [SerializeField] public string password = "1998";
    [SerializeField] private string input;

    [SerializeField] private string _DoorType;

    [SerializeField] private bool doorOpened = false;
    [SerializeField] private Animator animator;
    [SerializeField] private AudioSource btn_sound;
    [SerializeField] private AudioSource correct_password;
    [SerializeField] private AudioSource incorrect_password;
    [SerializeField] private DoorScript firstDoor;
    [SerializeField] private DoorScript secondDoor;

    public UIMessageSystem MessageSystem;
    private Outline outlineObj;
    private Camera mainCamera;
    private bool thisKeyPadCurrentlyActive = false;

    // Start is called before the first frame update
    void Start()
    {
        for (int i=0; i<UIButtons.Capacity; i++)
        {
            UIButtons[i].onClick.AddListener(UserClickedButton);
        }
        outlineObj = GetComponent<Outline>();
        mainCamera = Camera.main.GetComponent<Camera>();
    }

    void Update()
    {
        if (Keypad2D.activeSelf && Input.GetKeyDown(KeyCode.Q))
        {
            Keypad2D.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            FPSController.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;
            Time.timeScale = 1;
        }
    }

    public void HandleKeypad(RaycastHit hit)
    {
        if (SaveScript.InInspectMode || SaveScript.InInventoryMode || SaveScript.InOptionsMode) return;
        if (doorOpened) return;
        if (_DoorType.Equals("HallDoor") && SaveScript.ElectricityBoxExploded) return;

        // Currently moving an object
        if (SaveScript.HoldingObjectID != -1) return;

        if (Keypad2D.activeSelf && Input.GetKeyDown(KeyCode.Q))
        {
            Keypad2D.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            FPSController.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;
            Time.timeScale = 1;
        }

        else if (Keypad2D.activeSelf) return;


        if (hit.transform.gameObject.GetInstanceID() != gameObject.GetInstanceID()) return;
        if (gameObject.tag == "Outlineable") outlineObj.OutlineWidth = 10f;

        MessageSystem.DisableAll();
        MessageSystem.EnableSanity();
        MessageSystem.EnableKeypadMessage();

        if (Input.GetKeyDown(KeyCode.E))
        {
            SaveScript.InKeypadMode = true;

            FPSArms.SetActive(false);
            SaveScript.PreviouslyEquippedWeaponTag = "";

            MessageSystem.DisableKeypadMessage();
            MessageSystem.DisableSanityMessage();

            input = "";
            CodeMessage.text = "CODE: ";
            FeedbackMessage.text = "";

            thisKeyPadCurrentlyActive = true;
            Keypad2D.SetActive(true);
            FPSController.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = false;
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
        }
    }

    public void UserClickedButton()
    {
        if (!thisKeyPadCurrentlyActive) return;

        btn_sound.Play();

        string button = EventSystem.current.currentSelectedGameObject.name;

        if (input.Length == 4 && !button.Equals("C") && !button.Equals("Enter"))
        {
            return;
        }
        else if (!button.Equals("C") && !button.Equals("Enter"))
        {
            CodeMessage.text += button;
        }
        else FeedbackMessage.text = "";

        if (button.Equals("1")) input += "1";
        else if (button.Equals("2")) input += "2";
        else if (button.Equals("3")) input += "3";
        else if (button.Equals("4")) input += "4";
        else if (button.Equals("5")) input += "5";
        else if (button.Equals("6")) input += "6";
        else if (button.Equals("7")) input += "7";
        else if (button.Equals("8")) input += "8";
        else if (button.Equals("9")) input += "9";
        else if (button.Equals("0")) input += "0";
        else if (button.Equals("C"))
        {
            CodeMessage.text = "CODE: ";
            input = "";
        }
        else if (button.Equals("Enter"))
        {
            if (input == password)
            {

                if (firstDoor) firstDoor.DoorOpen();
                if (secondDoor) secondDoor.DoorOpen();
                if (!firstDoor && !secondDoor) animator.SetTrigger("Open");

                doorOpened = true;
                correct_password.Play();

                // Main door
                if (gameObject.name.Equals("Panel_maindoor") && input.Equals("4291"))
                {
                    PlayerPrefs.DeleteAll();
                    SceneManager.LoadScene(3);
                }
                if (gameObject.name.Equals("Panel_bedroom") && input.Equals("4667")) SaveScript.OpenedBedroomDoors = true;

                Keypad2D.SetActive(false);
                FPSController.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;
                Time.timeScale = 1;
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            else
            {
                incorrect_password.Play();
                FeedbackMessage.text = "Incorrect code.";
            }
        }
    }

    private void OnMouseExit()
    {
        if (Time.timeScale == 0) return;

        SaveScript.InKeypadMode = false;

        MessageSystem.DisableKeypadMessage();
        MessageSystem.EnableCrosshair();
        MessageSystem.EnableSanity();

        thisKeyPadCurrentlyActive = false;
        outlineObj.OutlineWidth = 0f;
    }
}
