using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

public class StartGameAnimation : MonoBehaviour
{
    public ShakeCamera CameraShake;
    public AudioSource EarRingingSound;
    public AudioSource HeartbeatSound;
    public AudioSource HeavyBreathingSound;
    public Animator HurtUI;
    public GameObject FPSController;
    public UIMessageSystem MessageSystem;

    [SerializeField] private Animator animator;
    void Start()
    {
        if (SaveScript.StartingAnimationFinished)
        {
            HeavyBreathingSound.gameObject.SetActive(false);
        }
        if (!SaveScript.StartingAnimationFinished)
        {
            MessageSystem.DisableAll();

            StartCoroutine(CameraShake.Shake(17f, 0.025f));

            EarRingingSound.Play();
            HeartbeatSound.Play();

            animator.SetBool("Cam_start", true);
            StartCoroutine("playSoundAndHurtUI");

            FPSController.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = false;
        }
    }

    IEnumerator playSoundAndHurtUI()
    {
        int sanity = 3;
        MessageSystem.SanityMessage.GetComponentInChildren<Text>().text = "3%";
        for (int i=0; i<8; i++)
        {
            sanity += 10;
            HeavyBreathingSound.Play();
            HurtUI.SetTrigger("Hurt");
            yield return new WaitForSeconds(HeavyBreathingSound.clip.length);
            MessageSystem.SanityMessage.GetComponentInChildren<Text>().text = sanity.ToString() + "%";
        }
        yield return new WaitForSeconds(7);
        FPSController.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;
        animator.SetBool("Cam_start", false);

        yield return new WaitForSeconds(1);
        SaveScript.StartingAnimationFinished = true;
        SaveScript.PlayerSanity = 100;

        MessageSystem.EnableSanity();
        MessageSystem.EnableCrosshair();

        MessageSystem.SanityMessage.GetComponentInChildren<Text>().text = "100%";
    }
}
