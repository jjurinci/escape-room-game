Note: These files are only for people who are using the Zombies in the "How to Make a Horror Survival Game in Unity" course.

You will need to modify the following scripts:

EnemyDamage.cs
EnemyMove.cs

In the screenshots you will see sections marked with the following comment:

//NEW!

Just add those extra lines of code to the scripts to modify them.


You will also have to create a new C sharp script called OnDead - the code is included in the screenshot.

Finally, there are screenshots of the settings and setup for the zombie characters to make them react like enemies in the horror game.